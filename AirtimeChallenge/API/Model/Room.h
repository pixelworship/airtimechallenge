//
//  Room.h
//  airtimeChallenge
//
//  Created by Matthew Mourlam on 12/26/17.
//  Copyright © 2017 Pixel Worship, LLC. All rights reserved.
//

@import Foundation;

@class Writing;

@interface Room : NSObject

@property (copy, nonatomic) NSString *roomId;
@property (assign, nonatomic) BOOL roomExplored;
@property (strong, nonatomic) NSMutableArray<Room *> *connectedRooms;
@property (assign, nonatomic) BOOL roomRead;
@property (copy, nonatomic) Writing *writing;

- (instancetype)initWithRoomId:(NSString *)roomId;

// Room has been both read and explored
- (BOOL)isProcessed;

@end
