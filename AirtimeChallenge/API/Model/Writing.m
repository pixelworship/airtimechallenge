//
//  Writing.m
//  airtimeChallenge
//
//  Created by Matthew Mourlam on 12/28/17.
//  Copyright © 2017 Pixel Worship, LLC. All rights reserved.
//

#import "Writing.h"

@implementation Writing

- (NSString *)description {
    return [NSString stringWithFormat:@" ** Writing Room ID: %@, writing: %@, order %@ ", self.roomId, self.writing ?: @"" , @(self.order)];
}

- (id)copyWithZone:(NSZone *)zone {
    Writing *newWriting = [[[self class] allocWithZone:zone] init];
    if (newWriting) {
        newWriting.roomId = self.roomId;
        newWriting.writing = self.writing;
        newWriting.order = self.order;
    }
    return newWriting;
}

@end
