//
//  DroneCommand.m
//  airtimeChallenge
//
//  Created by Matthew Mourlam on 12/26/17.
//  Copyright © 2017 Pixel Worship, LLC. All rights reserved.
//

#import "DroneCommand.h"

@implementation DroneCommand

- (instancetype)initWithCommand:(DroneCommandType)command roomId:(NSString *)roomId {
    if (self = [super init]) {
        // Make command id similar to current api format
        _commandId = [[[[NSUUID UUID] UUIDString] stringByReplacingOccurrencesOfString:@"-" withString:@""] lowercaseString];
        _command = command;
        _roomId = [roomId copy];
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"Command %@ Room Id: %@ Command ID: %@", self.command == DroneCommandTypeRead ? @"READ" : @"EXPLORE", self.roomId, self.commandId];
}

- (BOOL)isEqual:(DroneCommand *)object {
    return (self.commandId == object.commandId || (self.command == object.command && [self.roomId isEqualToString: object.roomId]));
}

@end
