//
//  Mission.m
//  airtimeChallenge
//
//  Created by Matthew Mourlam on 12/26/17.
//  Copyright © 2017 Pixel Worship, LLC. All rights reserved.
//

#import <ARKit/ARKit.h>

#import "Mission.h"

#import "DroneAPI.h"
#import "DroneCommand.h"
#import "Drone.h"
#import "Writing.h"
#import "Room.h"

@interface Mission () <DroneDelegate>
@property (strong, nonatomic) NSMutableArray *drones;
@property (strong, nonatomic) DroneAPI *droneAPI;

// Command queue to distribute to drones
@property (strong, nonatomic) NSMutableArray *commandQueue;

// Queues for various room states
@property (strong, nonatomic) NSMutableArray *processedRooms;
@property (strong, nonatomic) NSMutableArray *processingRooms;
@property (strong, nonatomic) NSMutableArray *unprocessedRooms;

@end

@implementation Mission

- (instancetype)init {
    if (self = [super init]) {
        _droneAPI = [[DroneAPI alloc] init];
        _drones = [[NSMutableArray alloc] init];

        _commandQueue = [[NSMutableArray alloc] init];

        _unprocessedRooms = [[NSMutableArray alloc] init];
        _processedRooms = [[NSMutableArray alloc] init];
        _processingRooms = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)beginMission {
    typeof (self) __weak weakSelf = self;
    [self.droneAPI start:^(Room *room, NSArray<Drone *> *drones, NSError *error) {
        if (error) {
            // FIXME: Add retry count? Currently retries infinitely after 1 second
            // Retry
            NSLog(@"Failed to /start. Retrying in 1 second. %@", error);
            [NSTimer scheduledTimerWithTimeInterval:1.0 repeats:NO block:^(NSTimer * _Nonnull timer) {
                [weakSelf beginMission];
            }];
            return;
        }

        // Successfully started mission, add room to unrprocessed
        [weakSelf.unprocessedRooms addObject:room];

        // Take roll call of available drones - one time event
        [weakSelf.drones addObjectsFromArray:drones];
        
        // Assign drone delegate
        for (Drone *drone in weakSelf.drones) {
            drone.delegate = self;
        }

        [weakSelf updateDroneQueue];
    }];
}

- (void)updateDroneQueue {
    // Grab all unprocessed rooms and convert them to EXPLORE and READ commands and add to command queue
    for (Room *room in self.unprocessedRooms) {
        // Explore room for connected rooms if not yet explored
        if (!room.roomExplored) {
            DroneCommand *command = [[DroneCommand alloc] initWithCommand:DroneCommandTypeExplore roomId:room.roomId];
            [self.commandQueue addObject:command];
        }

        // Read room if not yet read
        if (!room.roomRead) {
            DroneCommand *command = [[DroneCommand alloc] initWithCommand:DroneCommandTypeRead roomId:room.roomId];
            [self.commandQueue addObject:command];
        }

        // Add room to "processing rooms"
        [self.processingRooms addObject:room];
    }

    // Wait until we've added all rooms to process before removing rooms from unprocessed rooms now as we were previously reading it and didn't want to mutate it
    [self.unprocessedRooms removeObjectsInArray:self.processingRooms];

    // Begin processing drone command queue
    [self processDroneCommandQueue];
}

- (void)processDroneCommandQueue {
    NSLog(@"processDroneCommandsQueue: %@", self.commandQueue);

    // Find available drone
    for (Drone *drone in self.drones) {
        if ([drone isBusy] || [drone isCommandQueueFull]) {
            // Try next drone
            continue;
        }

        // Found available drone
        NSMutableArray *assignedCommands = [[NSMutableArray alloc] init];
        for (DroneCommand *command in self.commandQueue) {
            // Assign commands to available drone until full
            if ([drone addCommand:command]) {
                [assignedCommands addObject:command];
                continue;
            } else {
                break;
            }
        }

        [self.commandQueue removeObjectsInArray:assignedCommands];
    }

    NSLog(@"Finished processing commands on available drones - may have commands left over command queue: %@", self.commandQueue);

    BOOL dronesProcessing = NO;

    // FIXME: Process all assigned commands!
    for (Drone *drone in self.drones) {
        if (!drone.isBusy) {
            [drone processOpenCommands];
            if (!dronesProcessing) {
                dronesProcessing = [drone hasOpenCommands];
            }
        } else {
            dronesProcessing = YES;
        }
    }

    // Zero drones are processing rooms - we must be finished!
    if (!dronesProcessing) {
        NSLog(@"Completed scans of all rooms");
        [self processReport];
    }
}

- (Room *)processingRoomWithRoomId:(NSString *)roomId {
    for (Room *room in self.processingRooms) {
        if ([room.roomId isEqualToString:roomId]) {
            return room;
        }
    }
    return nil;
}

- (void)logProcessedRooms {
    NSLog(@"**** PROCESSED ROOMS START *****");
    for (Room *room in self.processedRooms) {
        NSLog(@"%@", [room description]);
    }
    NSLog(@"**** PROCESSED ROOMS END *****");
}

- (void)processReport {
    NSMutableArray *writings = [NSMutableArray new];
    // Iterate through processed rooms and create messages array
    for (Room *room in self.processedRooms) {
        if (room.writing && room.writing.writing.length > 0) {
            [writings addObject:room.writing];
        }
    }

    // Sort
    [writings sortUsingComparator:^NSComparisonResult(Writing *writing1, Writing *writing2) {
        if (writing1.order > writing2.order) {
            return NSOrderedDescending;
        } else if (writing1.order < writing2.order) {
            return NSOrderedAscending;
        }
        return NSOrderedSame;
    }];

    NSMutableString *completeWritings = [NSMutableString new];

    for (Writing *roomWriting in writings) {
        [completeWritings appendString:roomWriting.writing];
    }

    // Post report
    [self.droneAPI report:[completeWritings copy] completion:^(NSDictionary *response, NSError *error) {
        // FIXME: Would need to see failed response to handle error case better
        if (error) {
            NSLog(@"Failed to upload report: %@", response);
            if ([self.delegate respondsToSelector:@selector(failedMissionWithError:)]) {
                [self.delegate failedMissionWithError:error];
            }
            return;
        }

        // Successfully completed mission
        if ([self.delegate respondsToSelector:@selector(completedMission:)]) {
            [self.delegate completedMission:self];
            return;
        }
    }];
}

#pragma mark - DroneDelegate

- (void)drone:(Drone *)drone finishedCommandExploreConnections:(NSArray *)roomIds fromRoom:(NSString *)roomId {
    NSLog(@"Drone finishedCommandConnections: %@ %@", drone.droneId, roomIds);
    Room *room = [self processingRoomWithRoomId:roomId];
    
    if (!room) {
        NSLog(@"uhhh");
        return;
    }
    
    for (NSString *unprocessedRoomId in roomIds) {
        Room *roomToProcess = [[Room alloc] initWithRoomId:unprocessedRoomId];
        if (![self.processingRooms containsObject:roomToProcess] && ![self.unprocessedRooms containsObject:roomToProcess] && ![self.processedRooms containsObject:roomToProcess]) {
            [self.unprocessedRooms addObject:roomToProcess];
        }
    }
    
    room.roomExplored = YES;
    
    // If room is now processed, move from unprocessed to processed
    if ([room isProcessed]) {
        [self.processedRooms addObject:room];
        [self.processingRooms removeObject:room];
    }
}

-(void)drone:(Drone *)drone finishedCommandReadWriting:(Writing *)writing {
    NSLog(@"Drone finishedCommandReadWriting: %@ %@", drone.droneId, writing.writing);

    Room *room = [self processingRoomWithRoomId:writing.roomId];
    
    if (!room) {
        NSLog(@"uhhh");
    }
    
    if (writing.order != -1) {
        // Process writing
        room.writing = [writing copy];
    }
    room.roomRead = YES;
    
    // If room is now processed, move from unprocessed to processed
    if ([room isProcessed]) {
        [self.processedRooms addObject:room];
        [self.processingRooms removeObject:room];
    }
}

- (void)droneFinishedProcessingCommands:(Drone *)drone {
    NSLog(@"Drone finishedProcessingCommands: %@", @(self.processedRooms.count));
    [self updateDroneQueue];
}

@end
