//
//  DroneAPI.h
//  airtimeChallenge
//
//  Created by Matthew Mourlam on 12/26/17.
//  Copyright © 2017 Pixel Worship, LLC. All rights reserved.
//

@import Foundation;

@class Room, Drone, DroneCommand;

@interface DroneAPI : NSObject

// Start mission - returns a single room id plus array of drones
- (void)start:(void (^)(Room *room, NSArray<Drone *> *drones, NSError *error))completion;
// Run various list of commands on specific drone
- (void)runCommands:(NSArray<DroneCommand *> *)commands drone:(Drone *)drone completion:(void (^)(NSDictionary *response, NSError *error))completion;
// Run report with concatenated writings in order
- (void)report:(NSString *)combinedWritings completion:(void (^)(NSDictionary *response, NSError *error))completion;

@end
