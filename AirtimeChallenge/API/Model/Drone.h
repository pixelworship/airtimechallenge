//
//  Drone.h
//  airtimeChallenge
//
//  Created by Matthew Mourlam on 12/26/17.
//  Copyright © 2017 Pixel Worship, LLC. All rights reserved.
//

@import Foundation;

@class DroneCommand, Drone, Writing;

// Protocol methods for letting our delegate know when we've finished READing room, EXPLORING room and when we've finished processing all open COMMANDS
@protocol DroneDelegate<NSObject>
- (void)drone:(Drone *)drone finishedCommandReadWriting:(Writing *)writing;
- (void)drone:(Drone *)drone finishedCommandExploreConnections:(NSArray *)roomIds fromRoom:(NSString *)roomId;
- (void)droneFinishedProcessingCommands:(Drone *)drone;
@end

@interface Drone : NSObject

// Id for drone (hex format)
@property (copy, nonatomic, readonly) NSString *droneId;
// Delegate to handle command completion requests from drone
@property (assign, atomic) id<DroneDelegate> delegate;
@property (assign, nonatomic) BOOL isBusy;

- (instancetype)initWithDroneId:(NSString *)droneId;

// Is drone command queue full?
- (BOOL)isCommandQueueFull;
// Do we have any commands in command queue?
- (BOOL)hasOpenCommands;
// Add drone command to command queue
- (BOOL)addCommand:(DroneCommand *)command;
// Process any commands queue (up to max)
- (void)processOpenCommands;

@end
