//
//  Mission.h
//  airtimeChallenge
//
//  Created by Matthew Mourlam on 12/26/17.
//  Copyright © 2017 Pixel Worship, LLC. All rights reserved.
//

@import Foundation;

@class Mission;

@protocol MissionDelegate <NSObject>
@required
- (void)completedMission:(Mission *)mission;
@optional
- (void)failedMissionWithError:(NSError *)error;
@end

@interface Mission : NSObject

@property (weak, nonatomic) id<MissionDelegate> delegate;

/**
 Kicks off our drone mission for exploring and reading rooms using API

 @discussion We're currenting using a variety of queues (x3) to process rooms and distributing them in FIFO manner or buffer all commands to avaiable drones (not busy).
 Would it be more efficient to first Explore all rooms using all available drones THEN read each room vs current implementation of exploring and reading each unique room as it comes in
 It currently takes 18 requests to complete mission - we should explore more efficient ways of exploring and reading all rooms
 */
- (void)beginMission;

@end
