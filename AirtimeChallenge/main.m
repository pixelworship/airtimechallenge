//
//  main.m
//  AirtimeChallenge
//
//  Created by Matthew Mourlam on 12/30/17.
//  Copyright © 2017 Pixel Worship, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
