//
//  Drone.m
//  airtimeChallenge
//
//  Created by Matthew Mourlam on 12/26/17.
//  Copyright © 2017 Pixel Worship, LLC. All rights reserved.
//

#import "Drone.h"

#import "DroneAPI.h"
#import "DroneCommand.h"
#import "Writing.h"

static NSUInteger kMaxDroneCommands = 5;

@interface Drone()
@property (strong, nonatomic) DroneAPI *api;
@property (strong, nonatomic) NSMutableArray<DroneCommand *> *commands;
@end

@implementation Drone

- (instancetype)initWithDroneId:(NSString *)droneId {
    if (self = [super init]) {
        NSAssert(droneId, @"Error: No drone id. %s", __PRETTY_FUNCTION__);
        _droneId = droneId;
        _api = [[DroneAPI alloc] init];
        _commands = [[NSMutableArray alloc] init];
    }
    NSLog(@"%s droneId: %@", __PRETTY_FUNCTION__, droneId);
    return self;
}

// Is drone command queue full?
- (BOOL)isCommandQueueFull {
    return self.commands.count >= kMaxDroneCommands;
}

// Add drone command to command queue
- (BOOL)addCommand:(DroneCommand *)command {
    if (![self isCommandQueueFull]) {
        [self.commands addObject:command];
        NSLog(@"Assigned command: %@ to drone: %@", command, self.droneId);
        return YES;
    }

    NSLog(@"Assigned CANNOT assign command with RoomId: %@ to drone: %@ since it's FULL", command.roomId, self.droneId);

    // Drone command queue is full
    return NO;
}

// Process any drone commands
- (void)processOpenCommands {
    NSLog(@"processOpenCommands Drone: %@ - process commands called", self.droneId);
    typeof (self) __weak weakSelf = self;
    
    if (self.commands.count == 0) {
        NSLog(@"Process commands called with no commands to process");
        return;
    }

    // This drone is now busy
    self.isBusy = YES;

    // Generate our request
    [self.api runCommands:self.commands drone:self completion:^(NSDictionary *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                // FIXME: Handle error resopnse with timeout and retry
                NSLog(@"ERROR: %@", error);
                if ([weakSelf.delegate respondsToSelector:@selector(droneFinishedProcessingCommands:)]) {
                    [weakSelf.delegate droneFinishedProcessingCommands:weakSelf];
                }
                weakSelf.isBusy = NO;
                return;
            }

            if (response) {
                // Validate response
                if ([[response allKeys] count] == 0) {
                    // FIXME: Redo request AFTER delay
                    if ([weakSelf.delegate respondsToSelector:@selector(droneFinishedProcessingCommands:)]) {
                        [weakSelf.delegate droneFinishedProcessingCommands:weakSelf];
                    }
                    weakSelf.isBusy = NO;
                    return;
                }
            }

            // Handle response
            NSLog(@"processOpenCommands: %@", response);

            // Process command responses
            for (NSString *commandId in [response allKeys]) {
                DroneCommand *command = [self commandForId:commandId];

                if (response[@"error"] != nil) {
                    NSLog(@"Error in response");
                    // FIXME: Handle this gracefully
                    // FIXME: Do not remove command from queue until we know what kind of error took place
                    if ([weakSelf.delegate respondsToSelector:@selector(droneFinishedProcessingCommands:)]) {
                        [weakSelf.delegate droneFinishedProcessingCommands:weakSelf];
                    }
                    continue;
                }

                switch (command.command) {
                    case DroneCommandTypeRead: {
                        Writing *writing = [[Writing alloc] init];
                        writing.writing = response[commandId][@"writing"];
                        writing.order = [response[commandId][@"order"] intValue];
                        writing.roomId = command.roomId;
                        [weakSelf.commands removeObject:command];

                        if ([weakSelf.delegate respondsToSelector:@selector(drone:finishedCommandReadWriting:)]) {
                            [weakSelf.delegate drone:self finishedCommandReadWriting:writing];
                        }
                        break;
                    }
                    case DroneCommandTypeExplore: {
                        NSArray *connections = response[commandId][@"connections"];
                        [weakSelf.commands removeObject:command];
                        if ([weakSelf.delegate respondsToSelector:@selector(drone:finishedCommandExploreConnections:fromRoom:)]) {
                            [weakSelf.delegate drone:self finishedCommandExploreConnections:connections fromRoom:command.roomId];
                        }
                        break;
                    }
                }
            }
            
            weakSelf.isBusy = NO;
            if ([weakSelf.delegate respondsToSelector:@selector(droneFinishedProcessingCommands:)]) {
                [weakSelf.delegate droneFinishedProcessingCommands:weakSelf];
            }
        });
    }];
}

// Do we have any commands in command queue?
- (BOOL)hasOpenCommands {
    return self.commands.count > 0;
}

- (DroneCommand *)commandForId:(NSString *)commandId {
    for (DroneCommand *command in self.commands) {
        if ([command.commandId isEqualToString:commandId]) {
            return command;
        }
    }
    return nil;
}

@end
