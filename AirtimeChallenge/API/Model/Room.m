//
//  Room.m
//  airtimeChallenge
//
//  Created by Matthew Mourlam on 12/26/17.
//  Copyright © 2017 Pixel Worship, LLC. All rights reserved.
//

#import "Room.h"

#import "Writing.h"

@implementation Room

- (instancetype)initWithRoomId:(NSString *)roomId {
    if (self = [super init]) {
        NSAssert(roomId, @"Error: No room id. %s", __PRETTY_FUNCTION__);
        _roomId = roomId;
    }
    NSLog(@"%s roomId: %@", __PRETTY_FUNCTION__, _roomId);
    return self;
}

- (BOOL)isEqual:(Room *)room {
    return [self.roomId isEqualToString:room.roomId];
}

- (NSString *)description {
    return [NSString stringWithFormat: @"Room ID: %@ writing: %@ explored: %@ read: %@", self.roomId ? self.roomId : @"<null>", self.writing ? [self.writing description] : @"<null>", @(self.roomExplored), @(self.roomRead)];
}

// Room has been both read and explored
- (BOOL)isProcessed {
    return self.roomRead && self.roomExplored;
}

@end
