//
//  ViewController.m
//  airtimeChallenge
//
//  Created by Matthew Mourlam on 12/26/17.
//  Copyright © 2017 Pixel Worship, LLC. All rights reserved.
//

#import "MissionViewController.h"

#import "Mission.h"

@interface MissionViewController () <MissionDelegate>
@property (strong, nonatomic) Mission *mission;
@end

@implementation MissionViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Create and start mission
    self.mission = [[Mission alloc] init];
    self.mission.delegate = self;
    [self.mission beginMission];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MissionDelegate

- (void)completedMission:(Mission *)mission {
    NSLog(@"Mission completed!");
}



@end
