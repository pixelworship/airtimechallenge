//
//  Writing.h
//  airtimeChallenge
//
//  Created by Matthew Mourlam on 12/28/17.
//  Copyright © 2017 Pixel Worship, LLC. All rights reserved.
//

@import Foundation;

@interface Writing : NSObject

@property (copy, nonatomic) NSString *roomId;
@property (copy, nonatomic) NSString *writing;
@property (assign, nonatomic) NSInteger order;

@end
