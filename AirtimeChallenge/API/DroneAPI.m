//
//  DroneAPI.m
//  airtimeChallenge
//
//  Created by Matthew Mourlam on 12/26/17.
//  Copyright © 2017 Pixel Worship, LLC. All rights reserved.
//

#import "DroneAPI.h"

#import "Room.h"
#import "Drone.h"
#import "DroneCommand.h"

static NSString * const kBaseUrl = @"http://challenge2.airtime.com:10001/";
static NSString * const kStartUrl = @"start";
static NSString * const kDroneCommandUrl = @"drone/%@/commands";
static NSString * const kReportUrl = @"report";
static NSString * const kCommanderEmail = @"matt@mourlam.com";

@interface DroneAPI ()
@property (strong, nonatomic) NSURLSession *session;
@property (assign, atomic) NSUInteger numRequests;
@end

@implementation DroneAPI

- (instancetype)init {
    if (self = [super init]) {

    }
    return self;
}

// Start mission - returns a single room id plus array of drones
- (void)start:(void (^)(Room *room, NSArray<Drone *> *drones, NSError *error))completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", kBaseUrl, kStartUrl]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setValue:kCommanderEmail forHTTPHeaderField:@"x-commander-email"];
    request.HTTPMethod = @"GET";

    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            NSLog(@"ERROR: %s - %@", __PRETTY_FUNCTION__, error);
            if (completion) {
                completion(nil, nil, error);
            }
            return;
        }

        NSError *jsonError = NULL;
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
        if (jsonError) {
            NSLog(@"ERROR: %s - %@", __PRETTY_FUNCTION__, error);
            if (completion) {
                completion(nil, nil, jsonError);
            }
            return;
        }

        NSLog(@"jsonResponse: %@", jsonResponse);

        if (![[jsonResponse allKeys] containsObject:@"roomId"] || ![[jsonResponse allKeys] containsObject:@"drones"]) {
            NSLog(@"ERROR: Unexpected response from 'start' command - no 'drones' or 'roomId' keys in json response");
            if (completion) {
                completion(nil, nil, [NSError errorWithDomain:NSCocoaErrorDomain code:1 userInfo:nil]);
            }
            return;
        }

        // Populate room
        Room *room = [[Room alloc] initWithRoomId:jsonResponse[@"roomId"]];

        // Populate drones
        NSMutableArray *drones = [[NSMutableArray alloc] init];

        for (NSString *droneId in jsonResponse[@"drones"]) {
            Drone *drone = [[Drone alloc] initWithDroneId:droneId];
            [drones addObject:drone];
        }

        if (completion) {
            completion(room, drones, nil);
        }
    }];
    [task resume];
}

// Run various list of commands on specific drone
- (void)runCommands:(NSArray<DroneCommand *> *)commands drone:(Drone *)drone completion:(void (^)(NSDictionary *response, NSError *error))completion {
    NSString *urlString = [NSString stringWithFormat:@"%@%@", kBaseUrl, [NSString stringWithFormat: kDroneCommandUrl, drone.droneId]];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setValue:kCommanderEmail forHTTPHeaderField:@"x-commander-email"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    request.HTTPMethod = @"POST";

    self.numRequests++;
    NSLog(@"DroneAPI Num requests used for debugging efficiency: %@ %@", self, @(self.numRequests));

    // Generate body dictionary
    NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] init];

    // Create command dictionary body
    for (DroneCommand *command in commands) {
        switch (command.command) {
            case DroneCommandTypeRead:
                bodyDictionary[command.commandId] = @{ @"read" : command.roomId };
                break;
            case DroneCommandTypeExplore:
                bodyDictionary[command.commandId] = @{ @"explore" : command.roomId };
                break;
            default:
                NSAssert(false, @"Invalid drone command type");
                break;
        }
    }

    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:bodyDictionary options:0 error:nil];

    NSLog(@"JSON Body: %@", bodyDictionary);

// FIXME: Handle various drone error responses:
// 404 Not Found: droneId is invalid
// 400 Bad Request:
// {
// "error": "Drone <droneId> is busy already"
// }

    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            NSLog(@"ERROR: %s - %@", __PRETTY_FUNCTION__, error);
            if (completion) {
                completion(nil, error);
            }
            return;
        }

        NSError *jsonError = NULL;
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
        if (jsonError) {
            NSLog(@"ERROR: %s - %@", __PRETTY_FUNCTION__, error);
            if (completion) {
                completion(nil, jsonError);
            }
            return;
        }

        NSLog(@"jsonResponse: %@", jsonResponse);

        if (completion) {
            completion(jsonResponse, nil);
        }
    }];
    [task resume];
}

// Run report with concatenated writings in order
- (void)report:(NSString *)combinedWritings completion:(void (^)(NSDictionary *response, NSError *error))completion {
    NSString *urlString = [NSString stringWithFormat:@"%@%@", kBaseUrl, kReportUrl];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setValue:kCommanderEmail forHTTPHeaderField:@"x-commander-email"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    request.HTTPMethod = @"POST";

    // Generate body dictionary
    NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] init];
    bodyDictionary[@"message"] = combinedWritings;
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:bodyDictionary options:0 error:nil];

    NSLog(@"JSON Body: %@", bodyDictionary);

    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            NSLog(@"ERROR: %s - %@", __PRETTY_FUNCTION__, error);
            if (completion) {
                completion(nil, error);
            }
            return;
        }

        NSError *jsonError = NULL;
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
        if (jsonError) {
            NSLog(@"ERROR: %s - %@", __PRETTY_FUNCTION__, error);
            if (completion) {
                completion(nil, jsonError);
            }
            return;
        }

        NSLog(@"jsonResponse: %@", jsonResponse);

        if (completion) {
            completion(jsonResponse, nil);
        }
    }];
    [task resume];
}

@end
