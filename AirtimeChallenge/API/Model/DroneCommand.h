//
//  DroneCommand.h
//  airtimeChallenge
//
//  Created by Matthew Mourlam on 12/26/17.
//  Copyright © 2017 Pixel Worship, LLC. All rights reserved.
//

@import Foundation;

typedef NS_ENUM(NSUInteger, DroneCommandType) {
    DroneCommandTypeExplore = 0,
    DroneCommandTypeRead
};

@interface DroneCommand : NSObject

@property (copy, nonatomic) NSString *roomId;
@property (copy, nonatomic) NSString *commandId;
@property (assign, nonatomic) DroneCommandType command;

- (instancetype)initWithCommand:(DroneCommandType)command roomId:(NSString *)roomId;

@end
