HOW TO RUN

Simply open AirtimeChallenge.xcodeproj and Run on iOS simulator.
All output is displayed through debug cosole (no visual UI).

DESIGN DECISIONS

I decided to queue up commands for each discovered room and distribute them to the drones on a first-come first-serve basis.

I thought about potentially exploring each room, creating tree and then reading all the writing but didn't see any efficienty gains vs queuing up all explores + reads together.

APPLICATION OVERVIEW

Our "mission" is started on application launch which calls the /start api end point to retreieve the first room and list of available drones.

Each time a new room is found (which isn't currently being processed), that room is added to our unprocessed rooms queue.

Unprocessed rooms go from unprocessed to processed once both EXPLORE and READ commands are generated and added to our commands queue.

Commands are dequeued and sent in-order to next available drone. Available drones = !making request to api && it's own internal command queue does not exceed max (5 commands).

Each drone processes in internal command queue in blocks of 5 at a time to api. The drones have simple delegate methods attached letting our Mission class know it's finished EXPLORING room, READ writing in room or completed all commands in it's command queue.

Once a drone completes all it's commands, the entire cycle starts over (any unprocessed rooms? any open commands? distribute commands, etc).

Once there are no more room to process and none of the drones are "busy", we generate writing report to send to /report api end point.

NOTES:

In a larger app, I would typically use cocoapods, but being such a simple app, there wasn't really any need to complicate the running of the project.
Some typical pods I use are cocoalumberjack, sdwebimage, swiftlint and alamo.

I don't typically write unit tests in smaller apps but am familiar with test-driven development.